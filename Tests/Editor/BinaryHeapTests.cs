﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace NSJT.Collections.Editor.Tests
{
	public class BinaryHeapTests
	{
		[Test]
		public void AddRoot()
		{
			BinaryHeap<int> heap = new BinaryHeap<int>();
			heap.Add(1);

			Assert.AreEqual(1, heap.Root);
		}

		[Test]
		public void Count()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			BinaryHeap<int> heap = new BinaryHeap<int>(values);

			Assert.AreEqual(values.Length, heap.Count, "Heap is not the same size as added values");
		}

		[Test]
		public void Pop()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			int[] sorted = { 7, 5, 3, 2, 2, 1 };
			BinaryHeap<int> heap = new BinaryHeap<int>(values);

			for (int i = 0; i < sorted.Length; i++)
			{
				Assert.AreEqual(sorted[i], heap.Pop(), "Values are not popped from heap in order.");
			}
		}

		[Test]
		public void Add()
		{
			int[] values =   { 2, 1, 3, 2, 5, 7 };
			int[] greatest = { 2, 2, 3, 3, 5, 7 };
			BinaryHeap<int> heap = new BinaryHeap<int>();

			for (int i = 0; i < greatest.Length; i++)
			{
				heap.Add(values[i]);
				Assert.AreEqual(greatest[i], heap.Root, "Root of heap is not greatest value added.");
			}
		}

		[Test]
		public void RemovePresent()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			BinaryHeap<int> heap = new BinaryHeap<int>(values);

			Assert.True(heap.Remove(2), "2 could not be removed from heap despite being added.");
			Assert.AreEqual(values.Length - 1, heap.Count, "Remove did not decrease heap count");
		}

		[Test]
		public void RemoveMissing()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			BinaryHeap<int> heap = new BinaryHeap<int>(values);

			Assert.False(heap.Remove(4), "4 cannot be removed, since it is not present.");
			Assert.AreEqual(heap.Count, values.Length, "Heap count should remain the same");
		}

	}
}

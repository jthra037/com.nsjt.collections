﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.Linq;

namespace NSJT.Collections.Editor.Tests
{
	public class MinMaxHeapTests
	{
		[Test]
		public void MaxHeapOrder()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			int[] sorted = { 7, 3, 5, 1, 2, 2 };
			MaxHeap<int> heap = new MaxHeap<int>(values);

			bool result = heap.Zip(sorted, (x, y) => x == y).All(match => match);
			Assert.True(result, "Heap order does not match sorted");
		}

		[Test]
		public void MinHeapOrder()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			int[] sorted = { 1, 2, 3, 2, 5, 7 };
			MinHeap<int> heap = new MinHeap<int>(values);

			bool result = heap.Zip(sorted, (x, y) => x == y).All(match => match);
			Assert.True(result, "Heap order does not match sorted");
		}

		[Test]
		public void MaxHeapPopOrder()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			int[] sorted = { 7, 5, 3, 2, 2, 1 };
			MaxHeap<int> heap = new MaxHeap<int>(values);

			bool result = true;

			for(int i = 0; i < sorted.Length; i++)
			{
				result &= sorted[i] == heap.Pop();
			}

			Assert.True(result, "Heap order does not match sorted");
		}

		[Test]
		public void MinHeapPopOrder()
		{
			int[] values = { 2, 1, 3, 2, 5, 7 };
			int[] sorted = { 1, 2, 2, 3, 5, 7 };
			MinHeap<int> heap = new MinHeap<int>(values);

			bool result = true;

			for (int i = 0; i < sorted.Length; i++)
			{
				result &= sorted[i] == heap.Pop();
			}

			Assert.True(result, "Heap order does not match sorted");
		}
	}
}

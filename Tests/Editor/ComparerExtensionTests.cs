﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.Linq;

namespace NSJT.Collections.Editor.Tests
{

	using NSJT.Collections.Extensions;

	public class ComparerExtensionTests
	{
		[Test]
		public void ReverseCompare()
		{
			var comparer = Comparer<int>.Default;
			var reverseComparer = comparer.Reverse();

			var comparison = comparer.Compare(1, 3);
			var reverseComparison = reverseComparer.Compare(3, 1);

			Assert.AreEqual(comparison, reverseComparison);
		}

		[Test]
		public void ReverseCompareEqual()
		{
			var comparer = Comparer<int>.Default;
			var reverseComparer = comparer.Reverse();

			var comparison = comparer.Compare(5, 5);
			var reverseComparison = reverseComparer.Compare(5, 5);

			Assert.AreEqual(comparison, reverseComparison);
		}

		[Test]
		public void ReverseCompareDifferent()
		{
			var comparer = Comparer<int>.Default;
			var reverseComparer = comparer.Reverse();

			var comparison = comparer.Compare(5, 7);
			var reverseComparison = reverseComparer.Compare(5, 7);

			Assert.AreNotEqual(comparison, reverseComparison);
		}
	}
}

﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections.Generic;

namespace NSJT.Collections.Extensions
{
	public static class ComparerExtensions
	{
		public static IComparer<T> Reverse<T>(this Comparer<T> obj)
		{
			return ReverseComparer<T>.Set(obj);
		}
	}
}

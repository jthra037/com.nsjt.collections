﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections.Generic;

namespace NSJT.Collections
{
	public class ReverseComparer<T> : IComparer<T>
	{
		private static ReverseComparer<T> _instance;
		public static ReverseComparer<T> Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new ReverseComparer<T>(Comparer<T>.Default);
				}

				return _instance;
			}
		}

		private Comparer<T> comparer;

		private ReverseComparer(Comparer<T> comparer)
		{
			this.comparer = comparer;
		}

		public static ReverseComparer<T> Set(Comparer<T> comparer)
		{
			if (_instance != null && !_instance.comparer.Equals(comparer))
				_instance = new ReverseComparer<T>(comparer);

			return Instance;
		}

		public int Compare(T x, T y)
		{
			return comparer.Compare(x, y) * -1;
		}
	}
}

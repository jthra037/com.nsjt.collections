﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections.Generic;
using System.Linq;
using NSJT.Collections.Extensions;

namespace NSJT.Collections
{
    public class MinHeap<T> : BinaryHeap<T>
    {
        public MinHeap() : base(Enumerable.Empty<T>(), Comparer<T>.Default.Reverse()) { }
        public MinHeap(IEnumerable<T> collection) : base(collection, Comparer<T>.Default.Reverse()) { }
    }
}

﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NSJT.Collections
{
	[Serializable]
	public abstract class Map<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable, ICollection<TValue>, IEnumerable<TValue>, ISerializationCallbackReceiver
	{
		[SerializeReference]
		private TValue[] _values;

		private Dictionary<TKey, TValue> map;

		public int Count => ((ICollection<KeyValuePair<TKey, TValue>>)map).Count;

		public bool IsReadOnly => ((ICollection<KeyValuePair<TKey, TValue>>)map).IsReadOnly;

		public TValue this[TKey key]
		{
			get
			{
				return map[key];
			}
		}

		public Map()
		{
			map = new Dictionary<TKey, TValue>();
		}

		public Map(IEqualityComparer<TKey> comparer)
		{
			map = new Dictionary<TKey, TValue>(comparer);
		}

		public Map(IEnumerable<TValue> values, IEqualityComparer<TKey> comparer) : this(comparer)
		{
			AddRange(values);
		}

		public Map(IEnumerable<TValue> values) : this(values, default)
		{ }

		public abstract TKey GetKey(TValue value);

		public bool ContainsKey(TKey key)
		{
			return map.ContainsKey(key);
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			return map.TryGetValue(key, out value);
		}

		public void AddRange(IEnumerable<TValue> values)
		{
			foreach (TValue value in values)
			{
				Add(value);
			}
		}

		#region Interface Implementations
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<TKey, TValue>>)map).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<TKey, TValue>>)map).GetEnumerator();
		}

		public void Add(TValue value)
		{
			map.Add(GetKey(value), value);
		}

		public void Clear()
		{
			map.Clear();
		}

		public bool Contains(TValue item)
		{
			return map.Values.Contains(item);
		}

		public void CopyTo(TValue[] array, int index)
		{
			map.Values.CopyTo(array, index);
		}

		public bool Remove(TValue item)
		{
			return map.Remove(GetKey(item));
		}

		IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
		{
			return map.Values.GetEnumerator();
		}

		public void OnBeforeSerialize() { }

		public void OnAfterDeserialize()
		{
			AddRange(_values);
		}
		#endregion
	}
}
﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using System.Collections;
using System.Collections.Generic;

namespace NSJT.Collections
{
	public class PriorityNodeComparer<TValue, TPriority>
		: Comparer<PriorityNode<TValue, TPriority>>
		where TPriority : IComparable<TPriority>
	{
		public override int Compare(PriorityNode<TValue, TPriority> x, PriorityNode<TValue, TPriority> y)
		{
			return x.Priority.CompareTo(y.Priority);
		}
	}

	public class PriorityNode<TValue, TPriority> : IComparable<PriorityNode<TValue, TPriority>>, IComparable<TPriority>
		where TPriority : IComparable<TPriority>
	{
		public TValue Value;
		public TPriority Priority;

		public PriorityNode(TValue value, TPriority priority)
		{
			Value = value;
			Priority = priority;
		}

		public int CompareTo(PriorityNode<TValue, TPriority> other)
		{
			return Priority.CompareTo(other.Priority);
		}

		public int CompareTo(TPriority other)
		{
			return Priority.CompareTo(other);
		}

		public static implicit operator TValue(PriorityNode<TValue, TPriority> node)
		{
			return node.Value;
		}

		public static implicit operator (TValue Value, TPriority Priority)(PriorityNode<TValue, TPriority> node)
		{
			return (node.Value, node.Priority);
		}

		public static implicit operator PriorityNode<TValue, TPriority>((TValue Value, TPriority Priority) node)
		{
			return new PriorityNode<TValue, TPriority>(node.Value, node.Priority);
		}
	}

	public class PriorityQueue<TValue, TPriority>
		: ICollection<PriorityNode<TValue, TPriority>>, IEnumerable<PriorityNode<TValue, TPriority>>
		where TPriority : IComparable<TPriority>
	{
		private MaxHeap<PriorityNode<TValue, TPriority>> heap;

		public int Count => heap.Count;

		public bool IsReadOnly => ((ICollection<PriorityNode<TValue, TPriority>>)heap).IsReadOnly;

		public PriorityQueue(IEnumerable<PriorityNode<TValue, TPriority>> values)
		{
			heap = new MaxHeap<PriorityNode<TValue, TPriority>>(values);
		}

		public PriorityQueue(int capacity)
		{
			heap = new MaxHeap<PriorityNode<TValue, TPriority>>(capacity);
		}

		public PriorityQueue()
		{
			heap = new MaxHeap<PriorityNode<TValue, TPriority>>();
		}

		public PriorityNode<TValue, TPriority> Enqueue(TValue value, TPriority priority)
		{
			PriorityNode<TValue, TPriority> node = new PriorityNode<TValue, TPriority>(value, priority);
			Enqueue(node);
			return node;
		}

		// Returning input seems silly but it's for symmetry
		public PriorityNode<TValue, TPriority> Enqueue(PriorityNode<TValue, TPriority> node)
		{
			heap.Add(node);
			return node;
		}

		public PriorityNode<TValue, TPriority> Dequeue()
		{
			return heap.Pop();
		}

		public PriorityNode<TValue, TPriority> Peek()
		{
			return heap.Root;
		}

		public void Clear()
		{
			heap.Clear();
		}

		// Normally a priority queue would always be semi-ordered
		// but we are going to allow our elements to be mutated
		// which could effect the ideal order.
		public void Order()
		{
			MaxHeap<PriorityNode<TValue, TPriority>> newHeap
				= new MaxHeap<PriorityNode<TValue, TPriority>>(Count);

			while (heap.Count > 0)
			{
				newHeap.Add(heap.Pop());
			}

			heap = newHeap;
		}

		#region Interfaces implemented through "heap"
		public void Add(PriorityNode<TValue, TPriority> item)
		{
			heap.Add(item);
		}

		public bool Contains(PriorityNode<TValue, TPriority> item)
		{
			return heap.Contains(item);
		}

		public void CopyTo(PriorityNode<TValue, TPriority>[] array, int arrayIndex)
		{
			heap.CopyTo(array, arrayIndex);
		}

		public bool Remove(PriorityNode<TValue, TPriority> item)
		{
			return heap.Remove(item);
		}

		public IEnumerator<PriorityNode<TValue, TPriority>> GetEnumerator()
		{
			return heap.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return heap.GetEnumerator();
		}
		#endregion
	}
}

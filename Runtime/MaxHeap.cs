﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections.Generic;
using System.Linq;

namespace NSJT.Collections
{
	public class MaxHeap<T> : BinaryHeap<T>
	{
        public MaxHeap() : base(Enumerable.Empty<T>(), Comparer<T>.Default) { }
        public MaxHeap(IEnumerable<T> collection) : base(collection, Comparer<T>.Default) { }
        public MaxHeap(int capacity) : base(capacity, Comparer<T>.Default) { }
    }
}

﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using System.Collections.Generic;
using UnityEngine;

namespace NSJT.Collections
{
	[Serializable]
	public class IntMap<TValue> : Map<int, TValue>
	{
		public override int GetKey(TValue _)
		{
			return Count;
		}
	}
}
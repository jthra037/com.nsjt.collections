﻿// ========================================================================================
// Useful Collections - Non-standard collections, frequently used.
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NSJT.Collections
{
	public class BinaryHeap<T> : ICollection<T>, IEnumerable<T>, IEnumerable, IReadOnlyCollection<T>, ICollection
	{
		protected List<T> collection;
		private readonly IComparer<T> comparer;

		public T Root
		{
			get { return collection[0]; }
		}

		public int Count => ((ICollection<T>)collection).Count;

		public bool IsReadOnly => ((ICollection<T>)collection).IsReadOnly;

		public bool IsSynchronized => ((ICollection)collection).IsSynchronized;

		public object SyncRoot => ((ICollection)collection).SyncRoot;

		public BinaryHeap() : this(Enumerable.Empty<T>(), Comparer<T>.Default) { }
		public BinaryHeap(IEnumerable<T> collection) : this(collection, Comparer<T>.Default) { }
		public BinaryHeap(IComparer<T> comparer) : this(Enumerable.Empty<T>(), comparer) { }
		public BinaryHeap(IEnumerable<T> collection, IComparer<T> comparer)
		{
			this.collection = new List<T>(collection.Count());
			this.comparer = comparer;

			foreach(T element in collection)
			{
				Add(element);
			}
		}

		public BinaryHeap(int capacity) : this(capacity, Comparer<T>.Default) { }
		public BinaryHeap(int capacity, IComparer<T> comparer)
		{
			collection = new List<T>(capacity);
			this.comparer = comparer;
		}

		public void Add(T item)
		{
			collection.Add(item);

			int i = collection.Count - 1;
			int index;

			while (i > 0)
			{
				index = (i - 1) / 2;
				if (comparer.Compare(collection[i], collection[index]) > 0)
				{
					T temp = collection[i];
					collection[i] = collection[index];
					collection[index] = temp;
					i = index;
				}
				else
					break;
			}
		}

		public void DeleteRoot()
		{
			int i = collection.Count - 1;

			collection[0] = collection[i];
			collection.RemoveAt(i);

			i = 0;

			while (true)
			{
				int leftInd = 2 * i + 1;
				int rightInd = 2 * i + 2;
				int largest = i;

				if (leftInd < collection.Count)
				{
					if (comparer.Compare(collection[leftInd], collection[largest]) > 0)
						largest = leftInd;
				}

				if (rightInd < collection.Count)
				{
					if (comparer.Compare(collection[rightInd], collection[largest]) > 0)
							largest = rightInd;
				}

				if (largest != i)
				{
					T temp = collection[largest];
					collection[largest] = collection[i];
					collection[i] = temp;
					i = largest;
				}
				else
					break;
			}
		}

		public T Pop()
		{
			T result = collection[0];

			DeleteRoot();

			return result;
		}

		public bool Remove(T item)
		{
			if (collection.Count == 0)
			{
				return false;
			}

			bool found = false;
			Queue<T> popped = new Queue<T>(collection.Count);
			T current;
			while (collection.Count > 0 && !found)
			{
				current = Pop();

				if (comparer.Compare(current, item) == 0)
				{
					found = true;
				}
				else
				{
					popped.Enqueue(current);
				}
			}

			while (popped.Count > 0)
			{
				Add(popped.Dequeue());
			}

			return found;
		}

		#region Interfaces implemented through "collection"
		public void Clear()
		{
			collection.Clear();
		}

		public bool Contains(T item)
		{
			return collection.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			collection.CopyTo(array, arrayIndex);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return collection.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return collection.GetEnumerator();
		}

		public void CopyTo(Array array, int index)
		{
			((ICollection)collection).CopyTo(array, index);
		}
		#endregion
	}
}
